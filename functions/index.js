const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const admin = require('firebase-admin');
admin.initializeApp();

exports.addSendingTime = functions.database.ref('/com_list/{push_id}/data')
    .onCreate((snapshot, context) => {

        // TODO 2: 
        // 1. get cureent date data
        // 2. add this data to new column named "time" to current node
        // var today = new Date();
        // var dd = String(today.getDate()).padStart(2, '0');
        // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        // var yyyy = today.getFullYear();

        // today = mm + '/' + dd + '/' + yyyy;
        // document.write(today); 
        var dd=new Date();
        var day=dd.getDate();
        var month=dd.getMonth() + 1;
        var year=dd.getFullYear();

        var d=new Date().toLocaleString('zh-TW', {timeZone: 'Asia/Taipei'});  


        return snapshot.ref.parent.child('time').set(d);
        // document.write(day + "." + month + "." + year)
        // document.write("<br /><br />")
        // document.write(year + "/" + month + "/" + day)

    });
