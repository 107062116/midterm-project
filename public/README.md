# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : midterm project
* Key functions (add/delete)
    1. chat room
    
* Other functions (add/delete)
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：midterm-project-a2290.web.app

# Components Description : 
1. sign in page : 

    ![](https://i.imgur.com/Jz8hgCq.png)
    
    輸入信箱及密碼後，可透過下方三個按鈕來登入、Goole登入或註冊

2. choose room page : 

    ![](https://i.imgur.com/NfpV1fL.png)
    
    輸入正確密碼後，點選3個按鈕已進入不同的聊天室(密碼在console)

3. chat room page : 

    ![](https://i.imgur.com/twoS3m5.png)
    
    打字後按下Send即可於聊天室中發送訊息


# Other Functions Description : 

## Security Report (Optional)
